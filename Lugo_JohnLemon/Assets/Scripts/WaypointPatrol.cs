﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// Start is called before the first frame update
public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;

    int m_CurrentWaypointIndex;

    Transform player;
    bool aggro;

    public GameEnding gameEnding;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        navMeshAgent.SetDestination(waypoints[0].position);
    }

    void Update()
    {
        if (!aggro && navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
        }

    }

    public void SetAggro(bool set)
    {
        aggro = set;
        if (aggro)
            navMeshAgent.SetDestination(player.position);
        else
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform == player)
        {
            gameEnding.CaughtPlayer();
        }
    }
}
