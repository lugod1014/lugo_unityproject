﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    //the slider for the sprint bar
    public Slider sprintBar;
    //our variable we have for our sprint and the max amount we can have for it
    float runEnergy;
    float maxRunEnergy = 1.5f;

    // Start is called before the first frame update
    //At the start of the fram the sprit bar goes to full(max value) which then will also have the SetSprintSlider function
    void Start()
    {
        runEnergy = maxRunEnergy;
        sprintBar.maxValue = (float)(runEnergy);
        SetSprintSlider();

        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    private void Update()
    {
    //the key to use to sprint is space and if the run energy has a value more than 0 then the player will run 2 times faster
    // but at the same time will subtract from the runEnergy variable.
        if (Input.GetKey(KeyCode.Space) && runEnergy > 0f)
        {
            m_Animator.speed = 2f;
            runEnergy -= Time.deltaTime;
            SetSprintSlider();
        }
        else
        {
        //if spacekey isn't being held the speed of the player will go back to normal(1f) but then the runEnergy will add up
        // every second by .25f back to the runEnergy variable.
            m_Animator.speed = 1f;
            runEnergy += Time.deltaTime * .25f;
            if (runEnergy > maxRunEnergy)
                runEnergy = maxRunEnergy;
            SetSprintSlider();
        }
        //testing if sprintbar works
  
    }
    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }
    //function made so that the value of the sprint bar is equal to the runEnergy variable.
    void SetSprintSlider()
    {
        sprintBar.value = (float)(runEnergy);
    }
}
