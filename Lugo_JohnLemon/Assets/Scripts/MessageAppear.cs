﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageAppear : MonoBehaviour
{
    public GameObject player;
    public float messageDuration = 3f;
    bool m_PlayerInteraction;

    public GameObject message;

    // Start is called before the first frame update
    void OnTriggerEnter(Collider other)
    {
    //if the player which is tagged as Player and the object are together than the message will appear with the set active function.
        if (other.gameObject.tag == "Player")
        {
            m_PlayerInteraction = true;
            message.SetActive(true);
        }
        /*else if (other.gameObject.tag != "Player")
        {
            message.SetActive(false);
        }*/
    }

    private void OnTriggerExit(Collider other)
    {
        // if the player is not in the trigger box then the set active functionis false making the message dissapear.
        if (other.gameObject.tag == "Player")
        {
            message.SetActive(false);
        }
    }



    // Update is called once per frame
    /*void Update()
    {
        if (m_PlayerInteraction)
        {
            GhostTalk();
        }
    }

    void GhostTalk ()
    {

    }




        // StartCoroutine(ShowAndHide(other, 2.0f));
        // }
        // IEnumerator ShowAndHide(GameObject go, float delay)
        //{
        // go.SetActive(true);
        //  yield return new WaitForSeconds(delay);
        // go.SetActive(false);
        // }*/

}

